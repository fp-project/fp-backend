{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib where

  -- PV=C⋅[1−(1+i)−ni]

import GHC.Generics
import Aws.Lambda
import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as ByteString
import qualified RegularWithdraw

-- Input
data Event = Event
  {
    body :: String
  } deriving (Generic, FromJSON)

-- Output
data Response = Response
  { statusCode:: Int
  , headers :: Value
  , body :: String
  } deriving (Generic, ToJSON)


toResponse :: Double -> String
toResponse result = "{\"result\":" ++ (show result) ++ "}"

header :: Value
header = object ["Access-Control-Allow-Origin" .= ("*" :: String)]

handler :: Event -> Context -> IO (Either String Response)
handler Event{..} context = do
  case decode (ByteString.pack body) of
    Just inputRegularWithdraw ->
      pure $ Right Response
        { statusCode = 200
        , headers = header
        , body = toResponse (RegularWithdraw.calculateRegularWithdraw inputRegularWithdraw)
        }
    Nothing ->
      pure $ Right Response
        { statusCode = 400
        , headers = header
        , body = "{\"error\": true}"
        }
