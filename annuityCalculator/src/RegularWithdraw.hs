{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module RegularWithdraw where

import GHC.Generics
import Aws.Lambda
import Data.Aeson



data InputRegularWithdraw = InputRegularWithdraw
  { initialDeposit :: Double
  , interestRate :: Double
  , time :: Double
  , yearly :: Bool
  } deriving (Generic, FromJSON, ToJSON)

calculateN :: Bool -> Double -> Double
calculateN yearly time 
            | yearly = time * 12
            | otherwise = time


calculateInterest :: Bool -> Double -> Double
calculateInterest yearly i 
            | yearly = i / 100 / 12
            | otherwise = i / 100

calculateRegularWithdraw :: InputRegularWithdraw -> Double
calculateRegularWithdraw inputRegularWithdraw =
    initialDeposit inputRegularWithdraw /
    ((1-((1+ calculateInterest(yearly inputRegularWithdraw) (interestRate inputRegularWithdraw))**
    (-(calculateN(yearly inputRegularWithdraw) (time inputRegularWithdraw)))))/ 
    (calculateInterest (yearly inputRegularWithdraw) (interestRate inputRegularWithdraw)))