{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Mortgage where

import GHC.Generics
import Aws.Lambda
import Data.Aeson

-- Mortgage JSON object
-- to get from req body from AWS API Gateway to give to lambda
data InputMortgage = InputMortgage
  { principal :: Float
  , rate :: Float
  , numOfPayments :: Int
  } deriving (Generic, FromJSON, ToJSON)

calculateMortgage :: InputMortgage -> Float
calculateMortgage inputMortgage = p * ((r * (1 + r) ^ n)/((1 + r) ^ n - 1))
  where
    p = principal inputMortgage
    r = rate inputMortgage
    n = numOfPayments inputMortgage
-- mortgage formula
-- p = principal
-- r = monthly interest rate
-- n = number of month we'll be paying loan