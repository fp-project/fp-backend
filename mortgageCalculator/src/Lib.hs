{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Lib where

import GHC.Generics
import Aws.Lambda
import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as ByteString
import qualified Mortgage

-- Input
data Event = Event
  {
    body :: String
  } deriving (Generic, FromJSON)

-- Output
data Response = Response
  { statusCode:: Int
  , headers :: Value
  , body :: Float
  } deriving (Generic, ToJSON)

header :: Value
header = object ["Access-Control-Allow-Origin" .= ("*" :: String)]

handler :: Event -> Context -> IO (Either String Response)
handler Event{..} context = do
  case decode (ByteString.pack body) of
    Just inputMortgage ->
      pure $ Right Response
        { statusCode = 200
        , headers = header
        , body = Mortgage.calculateMortgage inputMortgage
        }
    Nothing ->
      pure $ Right Response
        { statusCode = 200
        , headers = header
        , body = -1
        }