{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module MonthlyPaymentAmount where

import GHC.Generics
import Aws.Lambda
import Data.Aeson

data InputForPaymentAmount = InputForPaymentAmount
  { loanAmount :: Double
  , loanTerm :: Double
  , interestRate :: Double
  } deriving (Generic, FromJSON, ToJSON)

monthlyRate annualRate = (1 + annualRate) ** (1 / 12) - 1

monthlyPaymentAmount :: InputForPaymentAmount -> Double
monthlyPaymentAmount inputForPaymentAmount = (loanAmount inputForPaymentAmount * monthlyRate (interestRate inputForPaymentAmount)) / (1 - (1 + monthlyRate (interestRate inputForPaymentAmount)) ** (- loanTerm inputForPaymentAmount))