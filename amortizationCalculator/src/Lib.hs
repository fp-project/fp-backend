{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Lib where

import GHC.Generics
import Aws.Lambda
import Data.Aeson
import qualified Data.ByteString.Lazy.Char8 as ByteString
import qualified MonthlyPaymentAmount

-- Input
data Event = Event
  { body :: String
  } deriving (Generic, FromJSON)

-- Output
data Response = Response
  { statusCode :: Int
  , headers :: Value
  , body :: Double
  } deriving (Generic, ToJSON)

header :: Value
header = object ["Access-Control-Allow-Origin" .= ("*" :: String)]  
  
handler :: Event -> Context -> IO (Either String Response)
handler Event{..} context = do
  case decode (ByteString.pack body) of
    Just inputForPaymentAmount ->
      pure $ Right Response
        { statusCode = 200
        , headers = header
        , body = MonthlyPaymentAmount.monthlyPaymentAmount inputForPaymentAmount
        }
    Nothing ->
      pure $ Right Response
        { statusCode = 400
        , headers = header
        , body = -1
        }