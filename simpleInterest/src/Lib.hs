{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Lib where

import GHC.Generics
import Data.Aeson
import Aws.Lambda
import qualified Data.ByteString.Lazy.Char8 as ByteString
import qualified SimpleInterest

-- Input
data Event = Event
  {
    body :: String
  } deriving (Generic, FromJSON)

-- Output
data Response = Response
  { statusCode:: Int
  , headers :: Value
  , body :: Double
  } deriving (Generic, ToJSON)

header :: Value
header = object ["Access-Control-Allow-Origin" .= ("*" :: String)]

handler :: Event -> Context -> IO (Either String Response)
handler Event{..} context = do
  case decode (ByteString.pack body) of
    Just simpleInterestInput ->
      pure $ Right Response
        { statusCode = 200
        , headers = header
        , body = SimpleInterest.calculateInterest simpleInterestInput 
        }
    Nothing ->
      pure $ Right Response
        { statusCode = 500
        , headers = header
        , body = -1
        }
