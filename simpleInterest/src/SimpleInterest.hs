{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module SimpleInterest where

import GHC.Generics
import Aws.Lambda
import Data.Aeson

data SimpleInterestInput = SimpleInterestInput
  { savings :: Double
  , interest :: Double
  , duration :: Double
  } deriving (Generic, FromJSON, ToJSON)

calculateInterest :: SimpleInterestInput -> Double
-- Parameters
-- [savings] in dollars
-- [interest] in per day
-- [duration] in days
calculateInterest simpleInterestInput = (savings simpleInterestInput) * (1 + (((interest simpleInterestInput) / 100) * duration simpleInterestInput))