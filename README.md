## Financial Calculator

Base URL: https://b5816fmvl6.execute-api.us-east-1.amazonaws.com/production/

## Endpoints

### `/simpleinterestcalculator`
Contain function that calculate interest by principal amount, interest rate, and time parameters.

### `/compoundinterestcalculator`
Contain function that calculate compounded interest by principal amount, interest rate, and time parameters.

### `/annuitycalculator`
Contain function that calculate regular withdraw amount of deposit by initial deposit, interest rate, and time paramters.

### `/amortizationcalculator`
Contain function that calculate monthly payment of amortization by loan amount, loan term, and interest rate parameters.

### `/mortgagecalculator`
Contain function that calculate monthly payment of mortgage by principal amount, interest rate, and number of payments parameters.

## Functional Style
Each endpoint used a handler that implemented Maybe Monad to determine whether the format of received data matched with data type each function used. If the result is Just dataType, then handler will process the data. Otherwise, handler will return error message.